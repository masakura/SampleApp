﻿using System;
using System.Linq;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using SampleApp.Infrastructures;
using SampleApp.Models;

namespace SampleApp
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class Program
    {
        public static void Main(string[] args)
        {
            var host = BuildWebHost(args);

            using (var scope = host.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetService<ApplicationDbContext>();

                if (db.Database.EnsureCreated())
                {
                    db.Books.AddRange(new Book {Name = "エンタープライズアプリケーションアーキテクチャパターン", Isbm = "4798105538"},
                        new Book {Name = "エリック・エヴァンスのドメイン駆動設計 ", Isbm = "4798121967"},
                        new Book {Name = "実践ドメイン駆動設計 (Object Oriented SELECTION) ", Isbm = "479813161X"},
                        new Book {Name = "現場で役立つシステム設計の原則 ~変更を楽で安全にするオブジェクト指向の実践技法", Isbm = "477419087X"});
                    db.SaveChanges();

                    var random = new Random();
                    var stocks = db.Books
                        .Select(b => new BookStockRow {BookId = b.Id, Stock = random.Next(0, 3) * 2});
                    db.BookStocks.AddRange(stocks);
                    db.SaveChanges();
                }
            }

            host.Run();
        }

        private static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
        }
    }
}