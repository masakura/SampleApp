﻿using Microsoft.AspNetCore.Mvc;
using SampleApp.Models;

namespace SampleApp.Controllers
{
    public sealed class HomeController : Controller
    {
        private readonly IProductsService _products;

        public HomeController(IProductsService products)
        {
            _products = products;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_products.All());
        }

        [HttpGet("{id}")]
        public IActionResult ById(int id)
        {
            return View(_products.GetProduct(id));
        }
    }
}