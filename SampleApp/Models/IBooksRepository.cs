﻿using System.Collections.Generic;

namespace SampleApp.Models
{
    public interface IBooksRepository
    {
        IEnumerable<Book> All();
        Book GetBook(int id);
    }
}