﻿namespace SampleApp.Models
{
    public sealed class Product
    {
        public Book Book { get; }
        public BookStock Stock { get; }

        public Product(Book book, BookStock stock)
        {
            Book = book;
            Stock = stock;
        }
    }
}