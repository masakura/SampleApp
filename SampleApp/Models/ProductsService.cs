﻿using System.Collections.Generic;
using System.Linq;

namespace SampleApp.Models
{
    internal sealed class ProductsService : IProductsService
    {
        private readonly IBooksRepository _books;
        private readonly IBookStocksRepository _stocks;

        public ProductsService(IBooksRepository books, IBookStocksRepository stocks)
        {
            _books = books;
            _stocks = stocks;
        }

        public Product GetProduct(int id)
        {
            var book = _books.GetBook(id);
            return Product(book);
        }

        public IEnumerable<Product> All()
        {
            return _books.All()
                .Select(Product)
                .ToArray();
        }

        private Product Product(Book book)
        {
            return new Product(book, _stocks.GetStock(book.Id));
        }
    }
}