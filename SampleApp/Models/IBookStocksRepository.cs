﻿namespace SampleApp.Models
{
    public interface IBookStocksRepository
    {
        BookStock GetStock(int bookId);
    }
}