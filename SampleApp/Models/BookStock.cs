﻿using System;

namespace SampleApp.Models
{
    public struct BookStock
    {
        private readonly int _value;

        public BookStock(int value)
        {
            if (value < 0) throw new ArgumentOutOfRangeException();

            _value = value;
        }

        public string AsText()
        {
            return Status.AsText(_value);
        }

        private StockStatus Status => StockStatus.Find(_value);

        private abstract class StockStatus
        {
            public abstract string AsText(int value);

            public static StockStatus Find(int value)
            {
                if (value < 0) throw new ArgumentOutOfRangeException();

                if (value == 0) return NoStock.Instance;
                if (value < 3) return FewStock.Instance;
                return MellowStock.Instance;
            }
        }

        private sealed class NoStock : StockStatus
        {
            private NoStock()
            {
            }
            
            public override string AsText(int value) => "在庫なし";
            public static StockStatus Instance = new NoStock();
        }
        
        private sealed class FewStock: StockStatus
        {
            private FewStock()
            {
            }

            public override string AsText(int value) => $"残り {value} 点";
            public static StockStatus Instance = new FewStock();
        }

        private sealed class MellowStock : StockStatus
        {
            private MellowStock()
            {
            }

            public override string AsText(int value) => "在庫あり";
            public static StockStatus Instance = new MellowStock();
        }
    }
}