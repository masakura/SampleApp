﻿using Microsoft.Extensions.DependencyInjection;

namespace SampleApp.Models
{
    public static class ModelsInfrastructuresServiceCollectionExtensions
    {
        public static void AddModels(this IServiceCollection services)
        {
            services.AddTransient<IProductsService, ProductsService>();
        }
    }
}