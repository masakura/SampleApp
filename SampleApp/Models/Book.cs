﻿namespace SampleApp.Models
{
    public sealed class Book
    {
        public int Id { get; set; }
        public string Isbm { get; set; }
        public string Name { get; set;  }
    }
}