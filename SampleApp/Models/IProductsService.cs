﻿using System.Collections.Generic;

namespace SampleApp.Models
{
    public interface IProductsService
    {
        IEnumerable<Product> All();
        Product GetProduct(int id);
    }
}