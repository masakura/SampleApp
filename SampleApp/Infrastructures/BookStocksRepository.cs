﻿using SampleApp.Models;

namespace SampleApp.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    internal sealed class BookStocksRepository : IBookStocksRepository
    {
        private readonly ApplicationDbContext _db;

        public BookStocksRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public BookStock GetStock(int bookId)
        {
            var stock = _db.BookStocks.Find(bookId);
            return new BookStock(stock.Stock);
        }
    }
}