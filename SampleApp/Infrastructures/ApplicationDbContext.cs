﻿using Microsoft.EntityFrameworkCore;
using SampleApp.Models;

namespace SampleApp.Infrastructures
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Book> Books { get; private set; }
        public DbSet<BookStockRow> BookStocks { get; private set; }
    }
}