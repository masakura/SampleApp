﻿using System.Collections.Generic;
using System.Linq;
using SampleApp.Models;

namespace SampleApp.Infrastructures
{
    internal sealed class BooksRepository : IBooksRepository
    {
        private readonly ApplicationDbContext _db;

        public BooksRepository(ApplicationDbContext db)
        {
            _db = db;
        }

        public IEnumerable<Book> All()
        {
            return _db.Books.ToArray();
        }

        public Book GetBook(int id)
        {
            return _db.Books.Find(id);
        }
    }
}