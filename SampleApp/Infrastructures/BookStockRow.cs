﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SampleApp.Infrastructures
{
    [Table("BookStocks")]
    // ReSharper disable once ClassNeverInstantiated.Global
    public sealed class BookStockRow
    {
        [Key]
        public int BookId { get; set; }
        public int Stock { get; set; }
    }
}