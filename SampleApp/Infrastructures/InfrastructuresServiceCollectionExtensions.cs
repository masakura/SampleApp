﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using SampleApp.Models;

namespace SampleApp.Infrastructures
{
    public static class InfrastructuresServiceCollectionExtensions
    {
        public static void AddInfrastructures(this IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options => options.UseInMemoryDatabase("App"));

            services.AddTransient<IBooksRepository, BooksRepository>();
            services.AddTransient<IBookStocksRepository, BookStocksRepository>();
        }
    }
}